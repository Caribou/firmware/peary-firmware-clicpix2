//                              -*- Mode: Verilog -*-
// Filename        : UtilitiesPackage.sv
// Description     : The package containing common data types, parameters, etc.
// Author          : Adrian Fiergolski
// Created On      : Mon Apr 24 15:07:02 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`ifndef UTILITIESPACKAGE_SV
 `define UTILITIESPACKAGE_SV

//Package: UtilitiesPackage
//The package contains common data type, parameters, etc.
package UtilitiesPackage;

   
   //Typedef: CLOCK_DOMAIN
   //Structure grouping signals related to a single clock domain
   typedef struct{
      logic      clk;    //clock signal
      logic      rst;    //positive reset
      logic      rstN;   //negative reset
      logic      locked; //in case signal comes from PLL         
   } CLOCK_DOMAIN;

endpackage // Utilities_Package

`endif //  `ifndef UTILITIESPACKAGE_SV
   
