-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.3.1 (lin64) Build 2035080 Fri Oct 20 14:20:00 MDT 2017
-- Date        : Tue Jan 23 12:48:43 2018
-- Host        : adrian-laptop running 64-bit Ubuntu 17.10
-- Command     : write_vhdl -force -mode synth_stub
--               /home/afiergol/clic/Caribou/peary-firmware/ipi-bd/caribou_top/ip/caribou_top_CLICpix2_0_2/caribou_top_CLICpix2_0_2_stub.vhdl
-- Design      : caribou_top_CLICpix2_0_2
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z045ffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity caribou_top_CLICpix2_0_2 is
  Port ( 
    AXI_araddr : in STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    AXI_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_arready : out STD_LOGIC;
    AXI_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_arvalid : in STD_LOGIC;
    AXI_awaddr : in STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    AXI_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_awready : out STD_LOGIC;
    AXI_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AXI_awvalid : in STD_LOGIC;
    AXI_bready : in STD_LOGIC;
    AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_bvalid : out STD_LOGIC;
    AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_rlast : out STD_LOGIC;
    AXI_rready : in STD_LOGIC;
    AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI_rvalid : out STD_LOGIC;
    AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI_wlast : in STD_LOGIC;
    AXI_wready : out STD_LOGIC;
    AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI_wvalid : in STD_LOGIC;
    C3PD_reset_n : out STD_LOGIC;
    C3PD_reset_p : out STD_LOGIC;
    CLICpix2_miso_n : in STD_LOGIC;
    CLICpix2_miso_p : in STD_LOGIC;
    CLICpix2_mosi_n : out STD_LOGIC;
    CLICpix2_mosi_p : out STD_LOGIC;
    CLICpix2_pwr_pulse_n : out STD_LOGIC;
    CLICpix2_pwr_pulse_p : out STD_LOGIC;
    CLICpix2_reset_n : out STD_LOGIC;
    CLICpix2_reset_p : out STD_LOGIC;
    CLICpix2_shutter_n : out STD_LOGIC;
    CLICpix2_shutter_p : out STD_LOGIC;
    CLICpix2_ss_n : out STD_LOGIC;
    CLICpix2_ss_p : out STD_LOGIC;
    CLICpix2_tp_sw_n : out STD_LOGIC;
    CLICpix2_tp_sw_p : out STD_LOGIC;
    SI5345_CLK_OUT8_clk_n : in STD_LOGIC;
    SI5345_CLK_OUT8_clk_p : in STD_LOGIC;
    TLU_busy_n : out STD_LOGIC;
    TLU_busy_p : out STD_LOGIC;
    TLU_reset_n : in STD_LOGIC;
    TLU_reset_p : in STD_LOGIC;
    TLU_trigger_n : in STD_LOGIC;
    TLU_trigger_p : in STD_LOGIC;
    Transceiver_RX_n : in STD_LOGIC;
    Transceiver_RX_p : in STD_LOGIC;
    Transceiver_refClk_clk_n : in STD_LOGIC;
    Transceiver_refClk_clk_p : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetN : in STD_LOGIC;
    control_irpt : out STD_LOGIC;
    pll_locked : out STD_LOGIC;
    receiver_irpt : out STD_LOGIC;
    spi_irpt : out STD_LOGIC
  );

end caribou_top_CLICpix2_0_2;

architecture stub of caribou_top_CLICpix2_0_2 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "AXI_araddr[30:0],AXI_arburst[1:0],AXI_arcache[3:0],AXI_arlen[7:0],AXI_arlock[0:0],AXI_arprot[2:0],AXI_arqos[3:0],AXI_arready,AXI_arregion[3:0],AXI_arsize[2:0],AXI_arvalid,AXI_awaddr[30:0],AXI_awburst[1:0],AXI_awcache[3:0],AXI_awlen[7:0],AXI_awlock[0:0],AXI_awprot[2:0],AXI_awqos[3:0],AXI_awready,AXI_awregion[3:0],AXI_awsize[2:0],AXI_awvalid,AXI_bready,AXI_bresp[1:0],AXI_bvalid,AXI_rdata[31:0],AXI_rlast,AXI_rready,AXI_rresp[1:0],AXI_rvalid,AXI_wdata[31:0],AXI_wlast,AXI_wready,AXI_wstrb[3:0],AXI_wvalid,C3PD_reset_n,C3PD_reset_p,CLICpix2_miso_n,CLICpix2_miso_p,CLICpix2_mosi_n,CLICpix2_mosi_p,CLICpix2_pwr_pulse_n,CLICpix2_pwr_pulse_p,CLICpix2_reset_n,CLICpix2_reset_p,CLICpix2_shutter_n,CLICpix2_shutter_p,CLICpix2_ss_n,CLICpix2_ss_p,CLICpix2_tp_sw_n,CLICpix2_tp_sw_p,SI5345_CLK_OUT8_clk_n,SI5345_CLK_OUT8_clk_p,TLU_busy_n,TLU_busy_p,TLU_reset_n,TLU_reset_p,TLU_trigger_n,TLU_trigger_p,Transceiver_RX_n,Transceiver_RX_p,Transceiver_refClk_clk_n,Transceiver_refClk_clk_p,aclk,aresetN,control_irpt,pll_locked,receiver_irpt,spi_irpt";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "CLICpix2,Vivado 2017.3.1";
begin
end;
