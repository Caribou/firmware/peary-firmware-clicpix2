//                              -*- Mode: Verilog -*-
// Filename        : AXI4StreamInterface.sv
// Description     : AXI4-Stream interface.
// Author          : Adrian Fiergolski
// Created On      : Mon Apr 24 14:20:26 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

//Interface: AXI4StreamInterface
//Description of the signal maybe find in Table 2-1 Interface signal list of the AMBA 4 AXI4-Stream Protocol Specification
interface AXI4StreamInterface#(parameter n=1, i = 8, d = 4, u = 8) (input logic aclk, input logic aresetN);

   logic tvalid;
   logic tready;
   logic [8*n-1:0] tdata;
   logic [n-1:0] tstrb;
   logic [n-1:0] tkeep;
   logic tlast;
   logic [i-1:0] tid;
   logic [d-1:0] tdest;
   logic [u-1:0] tuser;

   modport master(input aclk, input aresetN,
                  output tvalid, tdata, tstrb,
                  output tkeep, tlast, tid, tdest, tuser,
                  input tready );

   modport slave(input aclk, input aresetN,
                  input tvalid, tdata, tstrb,
                  input tkeep, tlast, tid, tdest, tuser,
                  output  tready );
   
   
endinterface // AXI4StreamInterface
