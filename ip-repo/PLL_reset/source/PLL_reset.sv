//                              -*- Mode: Verilog -*-
// Filename        : PLL_reset.sv
// Description     : The modue resets PLL.
// Author          : Adrian Fiergolski
// Created On      : Wed Aug  9 18:52:54 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module PLL_reset(
		 input logic clk,
		 input logic locked,
		 output logic reset);
   
   always_ff @( posedge clk ) begin : reset_ff
      logic [1:0] ff = 2'b10; //start with reset

      ff[1] <= ff[0];
      ff[0] <= locked;
      
      if( ff[1] && ~ff[0])
	reset <= 1'b1;
      else
	reset <= 1'b0;

   end // block: reset_ff
     
   
endmodule // PLL_reset
