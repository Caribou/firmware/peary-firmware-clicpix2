//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_receiver.sv
// Description     : The CLICpix2 receiver.
// Author          : Adrian Fiergolski
// Created On      : Tue Apr 11 16:12:18 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MENTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

`timescale 1ns / 1ps

module CLICpix2_receiver #(USE_CHIPSCOPE = 1, AXIS_TRANSCEIVER_N = 3, AXI_DATA_WIDTH = 32, AXI_ADDR_WIDTH = 3)
   (
    //Input Clock and data
    input logic 			   refClk_p,
    input logic 			   refClk_n,

   
    input logic 			   Transceiver_RX_p,
    input logic 			   Transceiver_RX_n,

    //Input system clock for reset FSM
    input logic 			   sysClk_in,

    //Interrupt
    //It generates an interrupt (rising edge) on falling edge of valid data.
    output logic 			   ip2intc_irpt,

    ///////////////////////
    //AXI4-Lite interface
    ///////////////////////

    //Write address channel
    input logic [AXI_ADDR_WIDTH-1 : 0] 	   awaddr,
    input logic [2 : 0] 		   awprot,
    input logic 			   awvalid,
    output logic 			   awready,

    //Write data channel
    input logic [AXI_DATA_WIDTH-1 : 0] 	   wdata,
    input logic [(AXI_DATA_WIDTH/8)-1 : 0] wstrb,
    input logic 			   wvalid,
    output logic 			   wready,
   
    //Write response channel
    output logic [1 : 0] 		   bresp,
    output logic 			   bvalid,
    input logic 			   bready,

    //Read address channel
    input logic [AXI_ADDR_WIDTH-1 : 0] 	   araddr,
    input logic [2 : 0] 		   arprot,
    input logic 			   arvalid,
    output logic 			   arready,

    //Read data channel
    output logic [AXI_DATA_WIDTH-1 : 0]    rdata,
    output logic [1 : 0] 		   rresp,
    output logic 			   rvalid,
    input logic 			   rready,
   
    input logic 			   aclk,
    input logic 			   aresetN
    );

   CLICpix2_receiver_ClockInterface clock();
   assign clock.refClk.p = refClk_p;
   assign clock.refClk.n = refClk_n;
   assign clock.sysClk.clk = sysClk_in;
   assign clock.axiClk.clk = aclk;
   assign clock.axiClk.rstN = aresetN;

   //Interfaces   
   AXI4StreamInterface #(AXIS_TRANSCEIVER_N, 0, 0, 0) transceiverData(clock.cdr.clk, clock.cdr.rstN);
   AXI4StreamInterface #(AXIS_TRANSCEIVER_N, 0, 0, 0) fifoStream(clock.axiClk.clk, clock.axiClk.rstN);
   logic [31:0] 			   fifoCounter;
   AXI4LiteInterface #(AXI_DATA_WIDTH, AXI_ADDR_WIDTH) axi(clock.axiClk.clk, clock.axiClk.rstN);


   //Modules
   CLICpix2_transceiver #(USE_CHIPSCOPE) clicpix2_transceiver( .serialInput_p(Transceiver_RX_p), .serialInput_n(Transceiver_RX_n), .* );

   DataFIFO fifo(  //slave interface
		   .s_axis_aresetn(transceiverData.aresetN), .s_axis_aclk(transceiverData.aclk),
		   .s_axis_tvalid(transceiverData.tvalid), .s_axis_tready(transceiverData.tready), .s_axis_tdata(transceiverData.tdata),

		   //master interface
		   .m_axis_aresetn(fifoStream.aresetN), .m_axis_aclk(fifoStream.aclk),
		   .m_axis_tvalid(fifoStream.tvalid), .m_axis_tready(fifoStream.tready), .m_axis_tdata(fifoStream.tdata),

		   //counters
		   .axis_data_count(), .axis_wr_data_count(),
		   .axis_rd_data_count(fifoCounter) );
   

   CLICpix2_AXIS2MM translator(  .* );

   //Interrupt
   assign ip2intc_irpt = ~transceiverData.tvalid;
   
   //Output AXI assignment
   assign axi.awaddr = awaddr;
   assign axi.awprot = awprot;
   assign axi.awvalid = awvalid;
   assign awready = axi.awready;

   //Write data channel
   assign axi.wdata = wdata;
   assign axi.wstrb = wstrb;
   assign axi.wvalid = wvalid;
   assign wready = axi.wready;
   
   //Write response channel
   assign bresp = axi.bresp;
   assign bvalid = axi.bvalid;
   assign axi.bready = bready;

   //Read address channel
   assign axi.araddr = araddr;
   assign axi.arprot = arprot;
   assign axi.arvalid = arvalid;
   assign arready = axi.arready;

   //Read data channel
   assign rdata = axi.rdata;
   assign rresp = axi.rresp;
   assign rvalid = axi.rvalid;
   assign axi.rready = rready;
   
endmodule // CLICpix2_receiver
