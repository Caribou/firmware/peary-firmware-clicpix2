# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"

  ipgui::add_param $IPINST -name "USE_CHIPSCOPE"
  ipgui::add_param $IPINST -name "AXIS_TRANSCEIVER_N"
  ipgui::add_param $IPINST -name "AXI_DATA_WIDTH"
  ipgui::add_param $IPINST -name "AXI_ADDR_WIDTH"

}

proc update_PARAM_VALUE.AXIS_TRANSCEIVER_N { PARAM_VALUE.AXIS_TRANSCEIVER_N } {
	# Procedure called to update AXIS_TRANSCEIVER_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AXIS_TRANSCEIVER_N { PARAM_VALUE.AXIS_TRANSCEIVER_N } {
	# Procedure called to validate AXIS_TRANSCEIVER_N
	return true
}

proc update_PARAM_VALUE.AXI_ADDR_WIDTH { PARAM_VALUE.AXI_ADDR_WIDTH } {
	# Procedure called to update AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AXI_ADDR_WIDTH { PARAM_VALUE.AXI_ADDR_WIDTH } {
	# Procedure called to validate AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.AXI_DATA_WIDTH { PARAM_VALUE.AXI_DATA_WIDTH } {
	# Procedure called to update AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AXI_DATA_WIDTH { PARAM_VALUE.AXI_DATA_WIDTH } {
	# Procedure called to validate AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.USE_CHIPSCOPE { PARAM_VALUE.USE_CHIPSCOPE } {
	# Procedure called to update USE_CHIPSCOPE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.USE_CHIPSCOPE { PARAM_VALUE.USE_CHIPSCOPE } {
	# Procedure called to validate USE_CHIPSCOPE
	return true
}


proc update_MODELPARAM_VALUE.USE_CHIPSCOPE { MODELPARAM_VALUE.USE_CHIPSCOPE PARAM_VALUE.USE_CHIPSCOPE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.USE_CHIPSCOPE}] ${MODELPARAM_VALUE.USE_CHIPSCOPE}
}

proc update_MODELPARAM_VALUE.AXIS_TRANSCEIVER_N { MODELPARAM_VALUE.AXIS_TRANSCEIVER_N PARAM_VALUE.AXIS_TRANSCEIVER_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AXIS_TRANSCEIVER_N}] ${MODELPARAM_VALUE.AXIS_TRANSCEIVER_N}
}

proc update_MODELPARAM_VALUE.AXI_DATA_WIDTH { MODELPARAM_VALUE.AXI_DATA_WIDTH PARAM_VALUE.AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.AXI_ADDR_WIDTH { MODELPARAM_VALUE.AXI_ADDR_WIDTH PARAM_VALUE.AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.AXI_ADDR_WIDTH}
}

