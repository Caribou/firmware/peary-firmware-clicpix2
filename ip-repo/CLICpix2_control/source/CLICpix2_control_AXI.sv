//                              -*- Mode: Verilog -*-
// Filename        : CLICpix2_control_AXI.sv
// Description     : The FSM handling AXI-Lite interface.
// Author          : Adrian Fiergolski
// Created On      : Mon May  8 16:58:23 2017
//
// Copyright Adrian Fiergolski <Adrian.Fiergolski@cern.ch> 2017
//
// This source file is licensed under the CERN OHL v. 1.2.
//
// You may redistribute and modify this souce file under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see
// the CERN OHL v.1.2 for applicable conditions.

module CLICpix2_control_AXI
  (
   CLICpix2_waveGeneratorControlInterface waveControl,
   FIFO_interface timestampsFIFO,
   CLICpix2_pinsInterface CLICpix2_pins,
   C3PD_pinsInterface C3PD_pins,
   AXI4LiteInterface.slave axi
   );


   enum logic [$bits(axi.awaddr)-1 : 0] {CLICPIX2_RESET ='h0,
					 CLICPIX2_WAVE_CONTROL = 'h4,
					 CLICPIX2_WAVE_EVENTS = 'h8,
					 C3PD_CONTROL = 'h8 + $size(waveControl.events) * 4, 
					 TIMESTAMPS_LSB = 'h8 + $size(waveControl.events) * 4 + 4, 
					 TIMESTAMPS_MSB = 'h8 + $size(waveControl.events) * 4 + 8 } REGISTER_ADDRESS;

   parameter CLICPIX2_RESET_BIT = 0;
   parameter CLICPIX2_WAVE_CONTROL_EN_BIT = 0;
   parameter CLICPIX2_WAVE_CONTROL_LOOP_BIT = 1;
   parameter C3PD_RESET_BIT = 0;

   
   struct {
      logic en;
      logic [$bits(axi.wdata) -1 : 0] d;
      logic [$bits(axi.rdata) -1 : 0] q;} registers[ REGISTER_ADDRESS.last()/4 + 1];

   //Registers assignment
   assign CLICpix2_pins.reset = registers[CLICPIX2_RESET / 4].q[CLICPIX2_RESET_BIT];
   assign waveControl.en_axi = registers[CLICPIX2_WAVE_CONTROL / 4 ].q[CLICPIX2_WAVE_CONTROL_EN_BIT];
   assign waveControl.loop = registers[CLICPIX2_WAVE_CONTROL / 4].q[CLICPIX2_WAVE_CONTROL_LOOP_BIT];
   for(genvar i = 0; i < $size(waveControl.events); i++) begin
      assign waveControl.events[i] = registers[CLICPIX2_WAVE_EVENTS / 4 + i].q;
   end
   assign C3PD_pins.reset = registers[C3PD_CONTROL /4 ].q[C3PD_RESET_BIT];

   //////////////
   //Write FSM
   ////////////////
   typedef enum {IDLE_W, WRITE_OK, WRITE_ERR} STATE_WRITE_T;
   STATE_WRITE_T stateW, stateW_n;

   always_comb begin : WRITE_COMB_FSM
      //Defaults
      axi.awready = 1'b0;
      axi.wready = 1'b0;
      axi.bresp = axi.OKAY;
      axi.bvalid = 1'b0;

      foreach(registers[i]) begin
	 registers[i].en = 1'b0;
	 registers[i].d = 'x;
      end

      stateW_n = stateW;      

      case (stateW)
	IDLE_W : begin
	   if(axi.wvalid) begin        //valid data
	      if( axi.awvalid ) begin  //valid address
		 if( axi.awaddr[$bits(axi.awaddr)-1 : 2] < REGISTER_ADDRESS.last() / 4 + 1 ) begin
		    registers[axi.awaddr[$bits(axi.awaddr)-1 : 2]].d = axi.wdata;
		    registers[axi.awaddr[$bits(axi.awaddr)-1 : 2]].en = 1'b1;

		    axi.awready = 1'b1;
		    axi.wready = 1'b1;
		    axi.bvalid = 1'b1;

		    if( ~axi.bready)
		      stateW_n = WRITE_OK;
		 end
		 else begin
		    axi.awready = 1'b1;
		    axi.wready = 1'b1;

		    axi.bvalid = 1'b1;
		    axi.bresp = axi.DECERR;

		    if( ~axi.bready)
		      stateW_n = WRITE_ERR;
		 end // else: !if( axi.awaddr[$bits(axi.awaddr)-1 : 2] < REGISTER_ADDRESS.num() )		 
	      end // if ( axi.awvalid )
	   end // if (axi.wvalid)
	end // case: IDLE

	WRITE_OK : begin
	   axi.bvalid = 1'b1;
	   if(axi.bready)
	     stateW_n = IDLE_W;
	end

	WRITE_ERR : begin
	   axi.bresp = axi.DECERR;
	   axi.bvalid = 1'b1;
	   
	   if(axi.bready)
	     stateW_n = IDLE_W;
	end
      endcase // case (state)
   end // block: WRITE_COMB_FSM

   always_ff @(posedge axi.aclk, negedge axi.aresetN) begin : WRITE_SEQ_FSM
      if ( ~ axi.aresetN ) begin
	 stateW <= IDLE_W;
	 for(int i = 0; i < TIMESTAMPS_LSB/4; i++)
	   registers[i].q <= '0;
      end
      else begin
	 stateW <= stateW_n;

	 for(int i = 0; i < TIMESTAMPS_LSB/4; i++)
	   if(registers[i].en) //store the output register
	     registers[i].q <= registers[i].d;
      end
   end // block: WRITE_SEQ_FSM

   assign timestampsFIFO.rst = ~ axi.aresetN;
   assign timestampsFIFO.rd_clk = axi.aclk;
   
   timestamp_FIFO timestamps(
			    .rst( timestampsFIFO.rst),
			    .wr_clk(timestampsFIFO.wr_clk),
			    .rd_clk(timestampsFIFO.rd_clk),
			    .din(timestampsFIFO.din),
			    .dout(timestampsFIFO.dout),
			    .wr_en(timestampsFIFO.din_en),
			    .rd_en(timestampsFIFO.dout_en),
			    .full(),
			    .empty(timestampsFIFO.dout_empty) );
   
   //////////////
   //READ FSM
   /////////////
   typedef enum {IDLE_R, READ_OK, READ_ERR} STATE_READ_T;
   STATE_READ_T stateR, stateR_n;

   always_comb begin : READ_COMB_FSM
      //Defaults
      axi.arready = 1'b1; //accept address immediately
      axi.rvalid = 1'b0;
      axi.rresp = axi.OKAY;
      axi.rvalid = 1'b0;
      axi.rdata = 'z;

      timestampsFIFO.dout_en = 1'b0;
      registers[ TIMESTAMPS_MSB / 4 ].q <= {timestampsFIFO.dout_empty, 12'b0,  timestampsFIFO.dout[$bits( timestampsFIFO.dout ) -1 :$bits(axi.rdata) ] };
      registers[ TIMESTAMPS_LSB / 4].q  <= timestampsFIFO.dout[$bits(axi.rdata)-1 : 0];

      stateR_n = stateR;      

      case (stateR)
	IDLE_R : begin
	   if( axi.arvalid ) begin  //valid address
	      if( axi.araddr[$bits(axi.araddr)-1 :2] < REGISTER_ADDRESS.last() / 4 + 1 ) begin
		 axi.rdata = registers[ axi.araddr[$bits(axi.araddr)-1 :2] ].q;

		 if( axi.araddr[$bits(axi.araddr)-1 :2] == TIMESTAMPS_MSB/4 )
		   timestampsFIFO.dout_en = 1'b1;
		 
		 axi.rvalid = 1'b1;

		 if(~axi.rready)
		   stateR_n = READ_OK;
	      end
	      else begin
		 axi.rvalid = 1'b1;

		 axi.rresp = axi.DECERR;

		 if(~axi.rready)
		   stateR_n = READ_ERR;
	      end // else: !if( axi.araddr[$bits(axi.araddr)-1 :2] < REGISTER_ADDRESS.num() )	      
	   end // if ( axi.arvalid )
	end // case: IDLE

	READ_OK : begin
	   axi.rvalid = 1'b1;
	   axi.rdata = registers[ axi.araddr[$bits(axi.araddr)-1 :2] ].q;
	   
	   if(axi.rready)
	     stateR_n = IDLE_R;
	end

	READ_ERR : begin
	   axi.rvalid = 1'b1;
	   axi.rresp = axi.DECERR;
	   
	   if(axi.rready)
	     stateR_n = IDLE_R;
	end
      endcase // case (state)
   end // block: READ_COMB_FSM

   always_ff @(posedge axi.aclk, negedge axi.aresetN) begin : READ_SEQ_FSM
      if ( ~ axi.aresetN ) begin
	 stateR <= IDLE_R;
      end
      else begin
	 stateR <= stateR_n;
      end
   end // block: READ_SEQ_FSM
   
endmodule // CLICpix2_control_AXI

  
