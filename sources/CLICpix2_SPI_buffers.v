`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.03.2017 09:05:11
// Design Name: 
// Module Name: CLICpix2_SPI_buffers
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CLICpix2_SPI_buffers(
    input  CLICpix2_ss,
    output CLICpix2_ss_p,
    output CLICpix2_ss_n,
    input  CLICpix2_mosi,
    output CLICpix2_mosi_p,
    output CLICpix2_mosi_n,
    output CLICpix2_miso,
    input  CLICpix2_miso_p,
    input  CLICpix2_miso_n,
    input CLICpix2_slow_clock,
    output CLICpix2_slow_clock_p,
    output CLICpix2_slow_clock_n
    );
    
    wire CLICpix2_slow_clock_oddr;
    
    OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_SS_inst (
       .O(CLICpix2_ss_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_ss_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_ss)      // Buffer input 
    );
    
    OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_MOSI_inst (
       .O(CLICpix2_mosi_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_mosi_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_mosi)      // Buffer input 
    );
   
    ODDR #(
        .DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE" or "SAME_EDGE" 
        .INIT(1'b0),    // Initial value of Q: 1'b0 or 1'b1
        .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
    ) ODDR_inst (
        .Q(CLICpix2_slow_clock_oddr),   // 1-bit DDR output
        .C(CLICpix2_slow_clock),   // 1-bit clock input
        .CE(1'b1), // 1-bit clock enable input
        .D1(1'b1), // 1-bit data input (positive edge)
        .D2(1'b0), // 1-bit data input (negative edge)
        .R(1'b0),   // 1-bit reset
        .S(1'b0)    // 1-bit set
    );
   
    OBUFDS #(
       .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
       .SLEW("SLOW")           // Specify the output slew rate
    ) OBUFDS_CLICpix2_MISO_inst (
       .O(CLICpix2_slow_clock_p),     // Diff_p output (connect directly to top-level port)
       .OB(CLICpix2_slow_clock_n),   // Diff_n output (connect directly to top-level port)
       .I(CLICpix2_slow_clock_oddr)      // Buffer input 
    );
    
    IBUFDS #(
       .DIFF_TERM("TRUE"),       // Differential Termination
       .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
       .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
    ) IBUFDS_CLICpix2_MISO_inst (
       .O(CLICpix2_miso),  // Buffer output
       .I(CLICpix2_miso_p),  // Diff_p buffer input (connect directly to top-level port)
       .IB(CLICpix2_miso_n) // Diff_n buffer input (connect directly to top-level port)
    );    
endmodule
